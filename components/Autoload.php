<?php

spl_autoload_register(function ($class) {
    (is_file(ROOT.'/components/'.$class.'.php')) ? require_once(ROOT.'/components/'.$class.'.php') : '';
    (is_file(ROOT.'/models/'.$class.'.php')) ? require_once(ROOT.'/models/'.$class.'.php') : '';
});
