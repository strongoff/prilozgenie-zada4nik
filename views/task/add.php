<?php include ROOT.'/views/layouts/header.php'; ?>

<div class="container theme-showcase" role="main">   
    <br> <br>
    <?php if ($errors) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
   
    <?php if ($result): ?>
        <div class="alert alert-success" role="alert">
            <p>Задача добавлена</p>
        </div>
    <?php else : ?>
    <div class="row">
          <div class="col-4">
            <h1>Форма добавления задачи</h1>
        <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data" id="form1" runat="server">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="addon-wrapping">Имя пользователя:</span>
              </div>
              <input type="text" placeholder="Имя пользователя" name="name" value="<?php echo $name; ?>" class="form-control" id="name">
            </div>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="addon-wrapping">E-mail:</span>
              </div>
              <input type="email" placeholder="E-mail" name="email" value="<?php echo $email; ?>" class="form-control" id="email">
            </div>

            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="addon-wrapping">Текст задачи:</span>
              </div>
              <textarea name="text" cols="40" rows="5" placeholder="Текст задачи" class="form-control" id="text"><?php echo $text; ?></textarea>
            </div>
 
            <div class="form-inline">
                <input type="submit" name="submit" value="Добавить" class="btn btn-outline-secondary btn-block"> 
            </div>  
        </form>
        </div>
      </div>  
    <?php endif; ?>
</div> 

<?php include ROOT.'/views/layouts/footer.php'; ?>