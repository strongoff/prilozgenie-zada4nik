<?php include ROOT.'/views/layouts/header.php'; ?>

<div class="container theme-showcase" role="main">   
    <div class="row">
        <div class="col-4"><div class="page-header"><h1>Редактирование задачи ID <?php echo $taskItem['task_id']; ?></h1></div>
        </div>
    </div>
    <?php if ($result): ?>
        <div class="alert alert-success" role="alert">
            <p>Успешно отредактировано</p>
        </div>
    <?php else : ?>
    <div class="row">
      <div class="col-4">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Имя пользователя:</span>
          </div>
          <input type="text" class="form-control" placeholder="<?php echo $taskItem['task_user_name']; ?>" readonly="readonly">
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon3">E-mail:      </span>
          </div>
          <input type="text" class="form-control" placeholder="<?php echo $taskItem['task_email']; ?>" readonly="readonly">
        </div>

        <form action="#" method="post" class="form-horizontal">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3">Текст задачи:      </span>
              </div>
              <textarea name="text" cols="40" rows="5" placeholder="Task text" class="form-control"><?php echo $taskItem['task_text']; ?></textarea>
            </div>
  
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="status" value="1" <?php if ($taskItem['task_status']) {
    echo 'checked';
} ?>>Выполнена
                </label>
            </div> 
            <div class="form-inline">
                <input type="submit" name="submit" value="Сохранить изменения" class="btn btn-outline-secondary btn-block">    
            </div>
        </form>
        </div>
       </div>   
    <?php endif; ?>
</div> 

<?php include ROOT.'/views/layouts/footer.php'; ?>