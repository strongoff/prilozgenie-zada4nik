<?php include ROOT.'/views/layouts/header.php'; ?>

<div class="container theme-showcase" role="main">   

    <div class="row">
        <div class="col-12">
    <form class="form-inline" action="/" method="post">
        <div class="form-group ">
            <label>Поле для сортировки: </label>
            <select class="form-control m-3" name="sortField">
                <option value="1" <?php if (1 == $_SESSION['sort_field']) {
    echo 'selected="selected"';
} ?>>Имя пользователя</option>
                <option value="2" <?php if (2 == $_SESSION['sort_field']) {
    echo 'selected="selected"';
} ?>>E-mail</option>
                <option value="3" <?php if (3 == $_SESSION['sort_field']) {
    echo 'selected="selected"';
} ?>>Статус</option>
            </select>
        </div>
        <div class="form-group ">
            <label>Тип сортировки: </label>
            <select class="form-control m-3" name="sortOrganize">
                <option value="1" <?php if (1 == $_SESSION['sort_organize']) {
    echo 'selected="selected"';
} ?>>По возрастанию</option>
                <option value="2" <?php if (2 == $_SESSION['sort_organize']) {
    echo 'selected="selected"';
} ?>>По убыванию</option>
            </select>   
        </div>
        <input class="btn btn-outline-secondary m-3" type="submit" name="submit" value="Сортировать">
    </form> 
   
    <table class="table table-bordered">
        <thead>       
            <tr>
                <td>Id</td>
                <td>Имя пользователя</td>
                <td>E-mail</td>
                <td>Текст задачи</td>
                <td>Отредактировано <br> администратором</td>
                <td>Статус</td>
                <?php if (!User::isGuest()) : ?>
                    <td></td>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($taskList as $taskItem) : ?>
                <tr class="<?php if ($taskItem['task_status']) {
    echo 'success';
} ?>">
                    <td><?php echo $taskItem['task_id']; ?></td>
                    <td><?php echo $taskItem['task_user_name']; ?></td>
                    <td><?php echo $taskItem['task_email']; ?></td>
                    <td><?php echo $taskItem['task_text']; ?></td>
                    <td>
                        <?php
                        if ($taskItem['task_edited_by_admin']) {
                            echo 'Да';
                        } else {
                            echo 'Нет';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($taskItem['task_status']) {
                            echo 'Выполнено';
                        } else {
                            echo 'Не выполнено';
                        }
                        ?>
                    </td>
                    <?php if (!User::isGuest()) : ?>
                        <td>
                            <a href="/task/edit/<?php echo $taskItem['task_id']; ?>" class="btn btn-outline-secondary" role="button">Редактировать</a>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>  
        </tbody>
    </table>
    <div class="col-12"><p><?php echo $pagination->get(); ?></p></div>
    </div> 
    </div> 
</div>   

<?php include ROOT.'/views/layouts/footer.php'; ?>