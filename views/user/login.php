<?php include ROOT.'/views/layouts/header.php'; ?>

<div class="container theme-showcase" role="main">   
    <div class="row"><div class="col-3"><div class="page-header"><h1>Авторизация</h1></div></div></div>  
    <?php if ($errors) : ?>
        <div class="alert alert-danger" role="alert">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="row">
    <div class="col-3">
    <form class="form-inline" action="#" method="post">  
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">Логин: </span>
      </div>
      <input type="text" class="form-control" placeholder="Логин" name="name" class="form-control" >
    </div>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">Пароль:</span>
      </div>
      <input type="text" class="form-control" placeholder="*****" name="password" class="form-control" >
    </div>
        <input type="submit" name="submit" value="Войти" class="btn btn-outline-secondary btn-lg btn-block">
    </form>
    </div>
    </div>
</div> 

<?php include ROOT.'/views/layouts/footer.php'; ?>


