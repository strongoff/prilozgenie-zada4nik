<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Приложение-задачник</title>
        <link href="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        
    </head>   
    <body>  
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link" href="/"><u>Список задач </u></a></li>
                <li class="nav-item"><a class="nav-link" href="/task/add"><u>Добавить задачу</u></a></li>
            </ul>

            <?php if (User::isGuest()) : ?>
                <a class="navbar-brand justify-content-end" href='/user/login'><u>Вход для администратора</u></a>
            <?php else : ?>
                <a class="navbar-brand justify-content-end" href='/user/logout'><u>Вы администратор. Выйти.</u></a>
            <?php endif; ?>
        </nav>