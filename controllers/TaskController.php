<?php

class TaskController
{
    public function actionIndex($page = 1, $sortField = 0, $sortOrganize = 1)
    {
        if (isset($_POST['submit'])) {
            $_SESSION['sort_field'] = intval($_POST['sortField']);
            $_SESSION['sort_organize'] = intval($_POST['sortOrganize']);
        }

        if (
                !isset($_SESSION['sort_field']) &&
                !isset($_SESSION['sort_organize'])
        ) {
            $_SESSION['sort_field'] = 0;
            $_SESSION['sort_organize'] = 1;
        }

        $taskList = Task::getTaskList($page, $_SESSION['sort_field'], $_SESSION['sort_organize']);
        $total = Task::getTotalTasks();
        $pagination = new Pagination($total, $page, Task::SHOW_DEFAULT, 'p');

        require_once ROOT.'/views/task/index.php';

        return true;
    }

    public function actionAdd()
    {
        if (isset($_POST['submit'])) {
            $errors = false;

            $name = htmlspecialchars($_POST['name']);
            $email = $_POST['email'];
            $text = htmlspecialchars($_POST['text']);

            if (!User::checkName($name)) {
                $errors[] = 'Вы не ввели имя';
            }

            if (!Task::checkEmail($email)) {
                $errors[] = 'E-mail введен неправильно';
            }

            if ('' == $text) {
                $errors[] = 'Вы не ввели текст задачи';
            }

            if (false == $errors) {
                $result = Task::add($name, $email, $text, $img);
            }
        }

        require_once ROOT.'/views/task/add.php';

        return true;
    }

    public function actionEdit($id)
    {
        User::checkAuth();

        $taskItem = Task::getTaskItemById($id);
        if (isset($_POST['submit'])) {
            $text = htmlspecialchars($_POST['text']);
            if (isset($_POST['status'])) {
                $status = (bool) $_POST['status'];
            }

            $result = Task::edit($id, $text, $status, true);
        }

        require_once ROOT.'/views/task/edit.php';

        return true;
    }
}
