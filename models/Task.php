<?php

class Task
{
    const SHOW_DEFAULT = 3;

    public static function getTaskList($page = 1, $sortField = 0, $sortOrganize = 1)
    {
        $sortField = intval($sortField);
        $sortOrganize = intval($sortOrganize);

        switch ($sortField) {
            case 0: $sortField = 'task_id';
                break;
            case 1: $sortField = 'task_user_name';
                break;
            case 2: $sortField = 'task_email';
                break;
            case 3: $sortField = 'task_status';
                break;
        }
        switch ($sortOrganize) {
            case 1: $sortOrganize = 'ASC';
                break;
            case 2: $sortOrganize = 'DESC';
                break;
        }

        $page = intval($page);
        $count = self::SHOW_DEFAULT;
        $offset = ($page - 1) * $count;

        $db = DB::getConnection();
        $query = 'SELECT * '
                .'FROM task_track '
                ."ORDER BY $sortField $sortOrganize "
                ."LIMIT $count "
                ."OFFSET $offset";
        $result = $db->query($query);

        if ($result) {
            foreach ($result as $row) {
                $taskList[] = array(
                    'task_id' => $row['task_id'],
                    'task_user_name' => $row['task_user_name'],
                    'task_email' => $row['task_email'],
                    'task_text' => $row['task_text'],
                    'task_status' => $row['task_status'],
                    'task_edited_by_admin' => $row['task_edited_by_admin'],
                );
            }
        }

        return $taskList;
    }

    public static function getTaskItemById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = DB::getConnection();

            $query = 'SELECT * '
                    .'FROM task_track '
                    .'WHERE task_id = '.$id;
            $result = $db->query($query);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $taskItem = $result->fetch();

            return $taskItem;
        }
    }

    public static function getTotalTasks()
    {
        $db = DB::getConnection();

        $query = 'SELECT COUNT(task_id) AS count '
                .'FROM task_track';
        $result = $db->query($query);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }

    public static function add($name, $email, $text)
    {
        $db = DB::getConnection();

        $query = 'INSERT INTO task_track SET '
                .'task_user_name = :name, '
                .'task_email = :email, '
                .'task_text = :text';
        $result = $db->prepare($query);
        $params = array(':name' => $name, ':email' => $email, ':text' => $text);

        return $result->execute($params);
    }

    public static function edit($id, $text, $status = false, $edited_by_admin = true)
    {
        $db = DB::getConnection();

        $query = 'UPDATE task_track SET 
        task_text = :text, 
        task_status = :status,
        task_edited_by_admin =:edited_by_admin 
        WHERE task_id = :id';

        $params = array(':id' => $id, ':text' => $text, ':status' => $status, ':edited_by_admin' => $edited_by_admin);
        $result = $db->prepare($query);

        return $result->execute($params);
    }

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        return false;
    }
}
